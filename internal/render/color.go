package render

import (
	"github.com/gdamore/tcell/v2"
)

var (
	MarkedColor      tcell.Color
)

type ColorFunc func(bool) tcell.Color
