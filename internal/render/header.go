package render

type HeaderColumn struct {
    Name       string
	Align      int
	Wide       bool
	Expansion  int
	Selectable bool
	Foreground ColorFunc
	Background ColorFunc
}

type Header []HeaderColumn
