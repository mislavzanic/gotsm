package render


type Fields []string

type Row struct {
	Fields Fields
	Marked bool
}

type Rows []Row

func (r *Row) Toggle() {
	r.Marked = !r.Marked
}
