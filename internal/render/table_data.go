package render

type SearchMode int

const (
	EMPTY      SearchMode = 0
	TOP_MOVIES            = 1
	TOP_SERIES            = 2
	SEARCH                = 4
	MARKED_TORRENTS       = 8
)

type TableData struct {
	Rows   Rows
	Header Header
	Mode   SearchMode
}

func (td TableData) Title() string {
	switch td.Mode {
	case TOP_MOVIES:      return "TOP MOVIES"
	case TOP_SERIES:      return "TOP SERIES"
	case SEARCH:          return "SEARCH"
	case MARKED_TORRENTS: return "MARKED FOR DOWNLOAD"
	default:              return "EMPTY"
	}
}
