package render

import (
	"codeberg.org/mislavzanic/gotsm/internal/types"
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

type Torrent struct {
	*types.Torrent
}

func NewRenderableTorrent(torrent *types.Torrent) *Torrent {
	return &Torrent{
		Torrent: torrent,
	}
}

func (t *Torrent) backgroundColor(marked bool) tcell.Color {
	if marked {
		return tcell.ColorDarkGreen
	}

	return tcell.ColorBlack
}

func (t *Torrent) Render(r *Row) {

}

// func (t *Torrent) Render(marked bool) []*tview.TableCell {

// 	return &tview.TableCell{
// 		Text: t.TorrentName,
// 		// Color: t.textColor(),
// 		BackgroundColor: t.backgroundColor(marked),
// 		Align: tview.AlignLeft,
// 	}
// }

func (t *Torrent) render(torrentDataType int) *tview.TableCell {
	return &tview.TableCell {
		// Text:
	}
}

// func (t *Torrent) base

func (t *Torrent) getCellColor() {

}
