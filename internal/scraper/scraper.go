package scraper

import (
	"regexp"
	"strconv"
	"strings"

	"github.com/gocolly/colly/v2"
	"codeberg.org/mislavzanic/gotsm/internal/types"
)

type Scraper struct {
    collector *colly.Collector
	Cache     *types.TorrentCache
}

func NewScraper() *Scraper {
	scraper := new(Scraper)
	scraper.collector = colly.NewCollector()
	scraper.collector.AllowURLRevisit = true
	scraper.Cache = types.NewCache()
	return scraper
}

func(s *Scraper) GetTorrents(url string) (types.TorrentMap, []string) {
	s.Cache.Clear()

	torrentSizes := []string{}
	torrentList, magnetLinks := []string{}, []string{}
	sele := []int{}

	s.collector.OnHTML("div", func(e *colly.HTMLElement) {
		switch class := e.Attr("class"); class {
		case "detName":
			torrentList = append(torrentList, strings.TrimSpace(e.Text))
		}
	})

	s.collector.OnHTML("a", func(e *colly.HTMLElement) {
		href := e.Attr("href")
		if match, _ := regexp.MatchString("magnet", href); match {
			magnetLinks = append(magnetLinks, strings.TrimSpace(e.Attr("href")))
			return
		}

		class := e.Attr("class")
		if match, _ := regexp.MatchString("detDesc", class); match {

			return
		}
	})

	s.collector.OnHTML("font", func(e *colly.HTMLElement) {
		attr := e.Attr("class")
		if match, _ := regexp.MatchString("detDesc", attr); match {
			re := regexp.MustCompile("\\d[. 0-9]* (MiB|GiB|KiB)")
			match := re.FindStringSubmatch(e.Text)
			if len(match) > 0 {
				torrentSizes = append(torrentSizes, match[0])
			}
		}
	})

	s.collector.OnHTML("td", func (e *colly.HTMLElement) {
		attr := e.Attr("align")
		if match, _ := regexp.MatchString("right", attr); match {
			if num, err := strconv.Atoi(e.Text); err == nil {
				sele = append(sele, num)
			}
		}
	})

	s.collector.Visit(url)


	for i, p := range torrentList {
		s.Cache.AddTorrent(
			&types.Torrent{
				MagnetLink: magnetLinks[i],
				TorrentSize: torrentSizes[i],
				Seeders: sele[2 * i],
				Leechers: sele[2 * i + 1],
				TorrentName: p,
				TorrentType: "",
		})
	}

	s.Cache.SetTorrentList(torrentList)

	return s.Cache.GetTorrentMap(), s.Cache.GetTorrentList()
}

func (scraper *Scraper) GetMagnets() types.TorrentMap {
	if scraper.Cache == nil {
		return nil
	}

	return scraper.Cache.GetTorrentMap()
}
