package gotsm

import (
	"context"
	"regexp"
	"strconv"
	"strings"

	"github.com/gdamore/tcell/v2"
	"github.com/hekmon/transmissionrpc/v2"
	"github.com/pkg/browser"

	"codeberg.org/mislavzanic/gotsm/internal/types"
	"codeberg.org/mislavzanic/gotsm/internal/ui"
	"codeberg.org/mislavzanic/gotsm/internal/scraper"
)

//TODO - menu with all categories
//     - number of pages when searching

const (
	PIRATE_BAY_URL = "https://thepiratebay10.org"
	TOP_MOVIES_URL = "/top/207"
	TOP_SERIES_URL = "/top/208"
)

const (
	DOWNLOAD_MODE int = 0
	WEB_MODE          = 1
)


type Tsm struct {
	tui             *ui.Tui
	scraper         *scraper.Scraper
	transmissionCli *transmissionrpc.Client

	searchQuery     string
	toDownload      *types.TorrentCache
}

func NewTsm() *Tsm {
	tsm := new(Tsm)

	tsm.searchQuery = ""
	tsm.toDownload = types.NewCache()
	tsm.transmissionCli, _ = transmissionrpc.New("127.0.0.1", "", "", nil)
	tsm.initScraper()
	tsm.initTui()

	return tsm
}

func (tsm *Tsm) initScraper() {
	tsm.scraper = scraper.NewScraper()
	tsm.scraper.GetTorrents(PIRATE_BAY_URL + TOP_MOVIES_URL)
}

func (tsm *Tsm) initTui() {
	tsm.tui = ui.NewTui()
	tsm.setTuiInputHandler().
		setTuiTableHandler().
		setInputDoneFunc().
		setInputChangeFunc()
}

func (tsm *Tsm)	setTuiTableHandler() *Tsm {
	handler := func(row, column int) {
		if magnets := tsm.scraper.GetMagnets(); magnets != nil {
			torrent := magnets[tsm.tui.GetTable().GetCell(row, column).Text]
			tsm.markTorrent(torrent)
			tsm.tui.ToggleCellColor(row, column, torrent)

			if tsm.tui.Mode & ui.MARKED_TORRENTS == 0 {
				tsm.tui.GetTable().Select(row + 1, column)
			}
		}
	}

	tsm.tui.SetTableSelectedFunc(handler)
	return tsm
}

func (tsm *Tsm) updateMarks() {
	for _, markedTorrentName := range tsm.toDownload.GetTorrentList() {
		if val, ok := tsm.scraper.Cache.GetTorrentMap()[markedTorrentName]; ok {
			val.Marked = ok
		}
	}
}

func (tsm *Tsm) markTorrent(torrent *types.Torrent) {
	torrent.Marked = !torrent.Marked

	if torrent.Marked {
		tsm.toDownload.AddTorrent(torrent)
	} else {
		tsm.toDownload.Remove(torrent)
	}

	if tsm.tui.Mode & ui.MARKED_TORRENTS == ui.MARKED_TORRENTS {
		tsm.tui.RenderTorrents(tsm.toDownload)
	}
}

func (tsm *Tsm) download() {
	for _, val := range tsm.toDownload.GetTorrentMap() {
		tsm.transmissionCli.TorrentAdd(context.TODO(), transmissionrpc.TorrentAddPayload{Filename: &val.MagnetLink})
		val.Marked = false
	}
	tsm.toDownload.Clear()
	tsm.tui.RenderTorrents(tsm.scraper.Cache)
}

func (tsm *Tsm) toggleList() {
	tsm.tui.Mode ^= ui.MARKED_TORRENTS

	if tsm.tui.Mode & ui.MARKED_TORRENTS == ui.MARKED_TORRENTS {
		tsm.tui.RenderTorrents(tsm.toDownload)
	} else {
		tsm.tui.RenderTorrents(tsm.scraper.Cache)
	}
}

func (tsm *Tsm) setTuiInputHandler() *Tsm {
	inputHandler := func (event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyESC {
			tsm.tui.FocusTable()
		} else {
			switch event.Rune() {
			case 'q': tsm.Stop()
			case 's': tsm.searchPage(event.Rune())
			case 'n': tsm.nextPage()
			case 'd': tsm.download()
			case 'l': tsm.toggleList()
			case 'p': tsm.prevPage()
			case '/': tsm.searchPage(event.Rune())
			case '1': tsm.getTopMovies()
			case '2': tsm.getTopSeries()
			}
		}
		return event
	}

	tsm.tui.SetTableInputHandler(inputHandler)
	return tsm
}

func (tsm *Tsm) setInputChangeFunc() *Tsm {
	changeFunc := func(r rune) func(string) {
		switch r {
		case '/': return func(text string) {
			cache := types.NewCache()
			tsm.tui.GetTable().Clear()
			torrentList := tsm.scraper.Cache.GetTorrentList()

			if tsm.tui.Mode & ui.MARKED_TORRENTS == ui.MARKED_TORRENTS {
				torrentList = tsm.toDownload.GetTorrentList()
			}

			for _, name := range torrentList {
				if match, _ := regexp.MatchString(strings.ToLower(text), strings.ToLower(name)); match {
					cache.AddTorrent(tsm.scraper.Cache.GetTorrent(name))
				}
			}
			tsm.tui.RenderTorrents(cache)
		}
		default: return func(text string) {}
		}
	}

	tsm.tui.SetInputChangeFunc(changeFunc)
	return tsm
}

func (tsm *Tsm) setInputDoneFunc() *Tsm {
	tableInputHandler := func (r rune) func(tcell.Key) {
		switch r {
		case 's': return func(tcell.Key) {

			tsm.tui.FocusTable()
			tsm.tui.ToggleSearchBar()
			tsm.tui.Page = 1

			if tsm.tui.Mode & ui.MARKED_TORRENTS == ui.MARKED_TORRENTS || len(tsm.tui.GetInputText()) <= 0 {
				return
			}

			tsm.searchQuery = tsm.tui.GetInputText()
			tsm.searchAndRender(PIRATE_BAY_URL + "/search/" + tsm.searchQuery + "/" + strconv.Itoa(tsm.tui.Page) + "/99/0")
			tsm.tui.Mode = ui.SEARCH
		}
		default: return func(tcell.Key) {tsm.tui.FocusTable(); tsm.tui.ToggleSearchBar()}
		}
	}
	tsm.tui.SetInputDoneFunc(tableInputHandler)
	return tsm
}

func (tsm *Tsm) Run() {
	tsm.tui.Run()
}

func (tsm *Tsm) Stop() {
	tsm.tui.Stop()
}

func (tsm *Tsm) searchAndRender(url string) {
	tsm.scraper.GetTorrents(url)
	tsm.updateMarks()
	tsm.tui.RenderTorrents(tsm.scraper.Cache)
	tsm.tui.FocusTable()
}

func (tsm *Tsm) getTopMovies() {
	tsm.tui.Mode = ui.TOP_MOVIES
	tsm.searchAndRender(PIRATE_BAY_URL + TOP_MOVIES_URL)
}

func (tsm *Tsm) getTopSeries() {
	tsm.tui.Mode = ui.TOP_SERIES
	tsm.searchAndRender(PIRATE_BAY_URL + TOP_SERIES_URL)
}

func (tsm *Tsm) searchPage(r rune) {
	tsm.tui.ToggleSearchBar()
	tsm.tui.FocusInput(r)
}

func (tsm *Tsm) nextPage() {
	if tsm.tui.Mode != ui.SEARCH {
		tsm.tui.Page = 0
		return
	}

	tsm.tui.Page++
	tsm.searchAndRender(PIRATE_BAY_URL + "/search/" + tsm.searchQuery + "/" + strconv.Itoa(tsm.tui.Page) + "/99/0")
}

func (tsm *Tsm) prevPage() {
	if tsm.tui.Mode != ui.SEARCH {
		tsm.tui.Page = 0
		return
	}

	if tsm.tui.Page > 1 {
		tsm.tui.Page--
		tsm.searchAndRender(PIRATE_BAY_URL + "/search/" + tsm.searchQuery + "/" + strconv.Itoa(tsm.tui.Page) + "/99/0")
	}
}

func (tsm *Tsm) researchMovie() {
	tsm.openIMDB(tsm.tui.GetTable().GetCell(tsm.tui.GetTable().GetSelection()).Text)
}

func (tsm *Tsm) openIMDB(movieTitle string) {
	browser.OpenURL("https://www.imdb.com/find?q=" + movieTitle + "&ref_=nv_sr_sm")
}
