package types

type TorrentMap map[string]*Torrent

type TorrentCache struct {
	torrents    TorrentMap
	torrentList []string
}

func NewCache() *TorrentCache {
	return &TorrentCache{
		torrents: map[string]*Torrent{},
		torrentList: []string{},
	}
}

func NewFullCache(torrents map[string]*Torrent, torrentList []string) *TorrentCache {
	return &TorrentCache{
		torrents: torrents,
		torrentList: torrentList,
	}
}

func (cache *TorrentCache) Clear() {
	cache.torrentList = []string{}
	cache.torrents = map[string]*Torrent{}
}

func (cache *TorrentCache) Remove(torrent *Torrent) {
	if len(cache.torrentList) == 1 {
		cache.Clear()
		return
	}

	for idx, item := range cache.torrentList {
		if item == torrent.TorrentName {
			cache.torrentList = append(cache.torrentList[:idx], cache.torrentList[idx+1:]...)
			break
		}
	}

	delete(cache.torrents, torrent.TorrentName)
}

func (cache *TorrentCache) AddTorrent(torrent *Torrent) {
	cache.torrentList = append(cache.torrentList, torrent.TorrentName)
	cache.torrents[torrent.TorrentName] = torrent
}

func (cache *TorrentCache) GetTorrent(name string) *Torrent {
	return cache.torrents[name]
}

func (cache *TorrentCache) GetTorrentMap() TorrentMap {
	return cache.torrents
}

func (cache *TorrentCache) GetTorrentList() []string {
	return cache.torrentList
}

func (cache *TorrentCache) SetTorrentList(torrentList []string) {
	cache.torrentList = torrentList
}
