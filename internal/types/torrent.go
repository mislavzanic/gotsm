package types

import (
	"strconv"
)

type Torrent struct {
	MagnetLink  string
	TorrentName string
	TorrentSize string
	Seeders     int
	Leechers    int
	TorrentType string

	Marked      bool
}

func (torrent Torrent) ToList() []string {
	return []string{
		torrent.TorrentSize,
		torrent.TorrentName,
		strconv.Itoa(torrent.Seeders),
		strconv.Itoa(torrent.Leechers),
	}
}
