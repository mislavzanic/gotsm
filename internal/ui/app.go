package ui

import (
	// "github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

type App struct {
    *tview.Application
}

func NewApp() *App {
	return &App{
		Application: tview.NewApplication(),
	}
}
