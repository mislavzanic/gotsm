package ui

import (
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"

	// "codeberg.org/mislavzanic/gotsm/internal/render"
	"codeberg.org/mislavzanic/gotsm/internal/render"
)

type Table struct {
	*tview.Table

	inputFunc  func(*tcell.EventKey) *tcell.EventKey
	selectFunc func(int, int)

	header []string
}

func NewTable() *Table {
	table := new(Table)
	table.Table = tview.NewTable()

	return table
}

func (table *Table) Render(data *render.TableData) {
	table.renderHeader(data.Header)
	table.render(data)
}

func (table *Table) render(data *render.TableData) {
	for row, rd := range data.Rows {
		table.buildRow(rd, data.Header, row + 1)
	}
}

func (table *Table) buildRow(r render.Row, h render.Header, row int) {
	for col, field := range r.Fields {
		tableCell := tview.NewTableCell(field)
		tableCell.SetExpansion(h[col].Expansion)
		tableCell.SetAlign(h[col].Align)
		tableCell.SetSelectable(h[col].Selectable)
		tableCell.SetBackgroundColor(h[col].Background(r.Marked))
		tableCell.SetTextColor(h[col].Foreground(r.Marked))

		table.SetCell(row, col, tableCell)
	}
}

func (table *Table) renderHeader(header render.Header) {
	for idx, item := range header {
		table.SetCell(0, idx, &tview.TableCell{
			Text: item.Name,
			Color: tcell.ColorYellow,
			BackgroundColor: tcell.ColorBlack,
			Align: tview.AlignLeft,
			NotSelectable: true,
		})
		if idx == 1 {
			table.GetCell(0, idx).SetExpansion(1)
		}
	}
}
