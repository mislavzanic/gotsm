package ui

import (
	"fmt"

	"codeberg.org/mislavzanic/gotsm/internal/types"
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

type SearchMode int

const (
	EMPTY      SearchMode = 0
	TOP_MOVIES            = 1
	TOP_SERIES            = 2
	SEARCH                = 4
	MARKED_TORRENTS       = 8
)

type Tui struct {
	*tview.Application

	table        *tview.Table
	mainFlex     *tview.Flex
	sideFlex     *tview.Flex
	bottomFlex   *tview.Flex
	input        *tview.InputField


	uriList   *tview.List
	keysList  *tview.List

	Mode        SearchMode
	Page        int

	inputChangeFunc   func(rune) func(string)
	inputDoneFunc     func(rune) func(tcell.Key)
	tableInputFunc    func(*tcell.EventKey) *tcell.EventKey
	tableSelectedFunc func(int, int)
}

func NewTui() *Tui {
	tui := new(Tui)

	tui.Application = tview.NewApplication()

	tui.Mode = TOP_MOVIES
	tui.Page = 1

	tui.inputChangeFunc   = nil
	tui.inputDoneFunc     = nil
	tui.tableInputFunc    = nil
	tui.tableSelectedFunc = nil


	tui.mainFlex = tview.NewFlex().SetDirection(tview.FlexRow)
	tui.bottomFlex  = tview.NewFlex().SetDirection(tview.FlexRow)
	tui.sideFlex = tview.NewFlex().SetDirection(tview.FlexColumn)

	tui.uriList = tview.NewList().ShowSecondaryText(false).
		SetSelectedBackgroundColor(tcell.ColorBlack).
		SetSelectedTextColor(tcell.ColorWhite).
		AddItem("Top Movies", "", '1', nil).
		AddItem("Top Series", "", '2', nil)

	tui.keysList= tview.NewList().ShowSecondaryText(false).
		SetMainTextStyle(tcell.StyleDefault.Bold(true)).
		SetSelectedBackgroundColor(tcell.ColorBlack).
		SetSelectedTextColor(tcell.ColorWhite).
		AddItem("Search for movies", "", 's', nil).
		AddItem("Search current list", "", '/', nil).
		AddItem("List marked torrents", "", 'l', nil).
		AddItem("Download marked torrents", "", 'd', nil).
		AddItem("Help", "", 'h', nil).
		AddItem("Quit", "", 'q', nil)

	tui.setupTable()

	tui.sideFlex.AddItem(tui.getFigletTable(), 0, 1, false).
			   	 AddItem(tui.uriList, 0, 1, false).
				 AddItem(tui.keysList, 0, 1, false)

	tui.bottomFlex.AddItem(tui.table, 0, 1, true)

	tui.mainFlex.AddItem(tui.sideFlex, 0, 3, false).
		         AddItem(tui.bottomFlex, 0, 16, true)


	tui.SetRoot(tui.mainFlex, true).EnableMouse(true)
	return tui
}

func (tui *Tui) GetTable() *tview.Table {
	return tui.table
}

func (tui *Tui) GetInputText() string {
	return tui.input.GetText()
}

func (tui *Tui) RenderTorrents(cache *types.TorrentCache) {
	tui.table.Clear()

	tui.renderHeader([]string{"Size", "Torrent", "Seeders", "Leechers"})
	for idx, torrentName := range cache.GetTorrentList() {
		tui.renderTorrent(idx + 1, cache.GetTorrent(torrentName))
	}
	tui.table.SetTitle(fmt.Sprintf("Torrents(%d)", len(cache.GetTorrentList())))
	tui.table.Select(1, 1)
}

func (tui *Tui) renderTorrent(row int, torrent *types.Torrent) {
	list := torrent.ToList()
	for column, item:= range list {
		tui.table.SetCell(row, column, &tview.TableCell{
			Text: item,
			Color: func(jdx int) tcell.Color {
				switch jdx {
				case 0: return tcell.ColorTeal
				case 1: return tcell.ColorWhite
				default: return tcell.ColorGreenYellow
				}
			}(column),
			BackgroundColor: func (jdx int) tcell.Color {
				switch jdx {
				case 1:
					if torrent.Marked && tui.Mode & MARKED_TORRENTS == 0 {
						return tcell.ColorDarkGreen
					}
					return tcell.ColorBlack
				default: return tcell.ColorBlack
				}
			}(column),
			Align: tview.AlignLeft,
			NotSelectable: column != 1,
		})
		if column == 1 {
			tui.table.GetCell(row, column).SetExpansion(1)
		}
	}
}

func (tui *Tui) renderHeader(header []string) {
	for idx, item := range header {
		tui.table.SetCell(0, idx, &tview.TableCell{
			Text: item,
			Color: tcell.ColorYellow,
			BackgroundColor: tcell.ColorBlack,
			Align: tview.AlignLeft,
			NotSelectable: true,
		})
		if idx == 1 {
			tui.table.GetCell(0, idx).SetExpansion(1)
		}
	}
}

func (tui *Tui) ToggleCellColor(row int, column int, torrent *types.Torrent) {
	if torrent.Marked {
		tui.GetTable().GetCell(row, column).SetBackgroundColor(tcell.ColorDarkGreen)
	} else {
		tui.GetTable().GetCell(row, column).SetBackgroundColor(tcell.ColorBlack)
	}
}

func (tui *Tui) setupInput() {
	tui.input = tview.NewInputField()
	tui.input.SetLabel("Search: ")
	tui.setupSearchBar(func() {tui.SetFocus(tui.table); tui.ToggleSearchBar()})
	tui.input.SetBorder(true).SetTitle("Search For Torrents")
}

func (tui *Tui) setupSearchBar(searchHandler func()) {
	tui.input.SetDoneFunc(func(key tcell.Key) {

		tui.SetFocus(tui.table)
		tui.Page = 1

		if len(tui.input.GetText()) <= 0 {
			return
		}

		searchHandler()
	})
}

func (tui *Tui) setupTable() {
	tui.table = tview.NewTable().SetFixed(1, 1).SetSelectable(true, false)
	tui.table.SetBorder(true).SetTitle("Torrents").SetBorderColor(tcell.ColorSkyblue).SetTitleColor(tcell.ColorSkyblue)
}

func (tui *Tui) SetTableSelectedFunc(handler func(int, int)) {
	tui.tableSelectedFunc = handler
	tui.table.SetSelectedFunc(tui.tableSelectedFunc)
	tui.table.SetDoneFunc(func(key tcell.Key) {
		if key == tcell.KeyESC {
			tui.Mode = 0
			tui.table.Clear()
			tui.table.SetTitle("Torrents")
			tui.FocusTable()
		}
	})
}

func (tui *Tui) SetTableInputHandler(handler func (event *tcell.EventKey) *tcell.EventKey) *Tui {
	tui.tableInputFunc = handler
	tui.table.SetInputCapture(tui.tableInputFunc)
	return tui
}

func (tui *Tui) SetInputChangeFunc(handler func(r rune) func(text string)) *Tui {
	tui.inputChangeFunc = handler
	return tui
}

func (tui *Tui) SetInputDoneFunc(handler func(r rune) func(key tcell.Key)) *Tui {
	tui.inputDoneFunc = handler
	return tui
}

func (tui *Tui) FocusInput(r rune) {
	tui.SetFocus(tui.input)

	tui.input.SetChangedFunc(tui.inputChangeFunc(r)).
		SetDoneFunc(tui.inputDoneFunc(r))
}

func (tui *Tui) FocusTable() {
	tui.SetFocus(tui.table)
}

func (tui *Tui) ToggleSearchBar() {
	if tui.bottomFlex.GetItemCount() == 1 {
		tui.setupInput()
		tui.bottomFlex.Clear().
			AddItem(tui.input, 3, 0, false).
			AddItem(tui.table, 0, 1, true)
		return
	}

	tui.bottomFlex.RemoveItem(tui.input)
}

func (tui *Tui) getFigletTable() *tview.Table {
	table := tview.NewTable()

	list := []string{
		"  ____     _____ ____  __  __ ",
		" / ___| __|_   _/ ___||  \\/  |",
		"| |  _ / _ \\| | \\___ \\| |\\/| |",
		"| |_| | (_) | |  ___) | |  | |",
		" \\____|\\___/|_| |____/|_|  |_|",
	}

	for idx, item := range list {
		table.SetCell(idx, 0, &tview.TableCell{
			Text: item,
			Color: tcell.ColorRed,
			BackgroundColor: tcell.ColorBlack,
			Align: tview.AlignLeft,
		})
	}
	return table
}

func (tui *Tui) printHelp() {

}
