package main

import (

	"codeberg.org/mislavzanic/gotsm/internal"
)

func main() {
	tsm := gotsm.NewTsm()
	tsm.Run()
}
